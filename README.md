# WebView

WebView is a striped down, cross platform browser intended to serve as a universal client for web applications.

It includes features that are relevant only to web applications and is fully customizable for the specific project it is paired with. 

In the future, an accompanying management application will be available which will allow non technical users to create WebView wrappers for websites of their choosing. These configurations can be uploaded and shared with other users, making the process even simpler.    