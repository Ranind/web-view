#ifndef QT_NO_WIDGETS
#include <QtWidgets/QApplication>
typedef QApplication Application;
#else
#include <QtGui/QGuiApplication>
typedef QGuiApplication Application;
#endif
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtWebEngine/qtwebengineglobal.h>

#include <QFileInfo>

#include "utils.h"

#include <sysexits.h>
#include <iostream>
#include <string>

#include <QDebug>
#include <QJsonParseError>

void showInstructions();
QUrl startupConfig(QString & configPath, bool & allowMalformedJson);

bool debug_enabled = true;
bool output_enabled = true;

void messageCallback(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        if(debug_enabled && output_enabled)
            fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtInfoMsg:
        if(output_enabled)
            fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtWarningMsg:
        if(output_enabled)
            fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        if(output_enabled)
            fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        if(output_enabled){
            fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
            abort();
        }
    }
}

int main(int argc, char *argv[])
{
    // Set the custom message callback
    qInstallMessageHandler(messageCallback);

    Application app(argc, argv);

    // Filepath for JSON config file
    QString configPath = "";

    // Flag for handling malformed json
    bool allowMalformedJson;

    // Run the startup config
    QUrl baseURL = startupConfig(configPath, allowMalformedJson);

    // Load the JSON config file if possible
    QString rawJSON;
    if(configPath.length() > 0){
        qDebug() << "Reading JSON config";
        QFile configFile;
        configFile.setFileName(configPath);
        configFile.open(QIODevice::ReadOnly | QIODevice::Text);
        rawJSON = configFile.readAll();
    }
    else{
        qDebug() << "No JSON config filepath given. Skipping.";
        rawJSON = "{ \"No\":\"Config File\" }";
    }

    // Attempt to parse the JSON
    QJsonParseError err;
    QJsonDocument json = QJsonDocument::fromJson(rawJSON.toUtf8(), &err);
    QJsonObject config;

    // Errors occurred during parse
    if(err.error != QJsonParseError::NoError){
        // malformed json will be corrected automatically
        if(allowMalformedJson){
            qWarning() << "JSON config is invalid -> " << err.errorString() <<
                        "\nUsing default settings...";
        }
        // malformed json will NOT be corrected, exit with config error
        else{
            qFatal(QString("JSON config is invalid -> " + err.errorString()).toLatin1().data());
            exit(EX_CONFIG);
        }
    }
    // Json parse completed without errors
    else{
        config = json.object();
    }

    // Merge user config with default settings
    Utils::mergeSettings(config);

    // Insert the base url for use in qml styling
    config.insert("baseURL", QJsonValue(baseURL.toString()));

    // Initialize the web engine
    QtWebEngine::initialize();

    // QML Setup
    QQmlApplicationEngine engine;
    Utils utils(config);
    engine.rootContext()->setContextProperty("utils", &utils);

    qDebug() << "Engine Loading ApplicationRoot.qml";

    if(config.value("gui_tabs").toBool())
        engine.load(QUrl(QStringLiteral("qrc:/ApplicationRoot.qml")));
    else
        engine.load(QUrl(QStringLiteral("qrc:/TablessApplicationRoot.qml")));

    qDebug() << "Invoking webengine load with result from startupConfig()";

    // Load the initial/startup Url
    QString title = utils.getSetting("title").toString();

    qDebug() << "CONFIG COMPLETED";

    QMetaObject::invokeMethod(engine.rootObjects().first(), "load", Q_ARG(QVariant, baseURL), Q_ARG(QVariant, title));

    qDebug() << "Starting WebViewer!";

    return app.exec();
}

