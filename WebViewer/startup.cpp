#ifndef QT_NO_WIDGETS
#include <QtWidgets/QApplication>
typedef QApplication Application;
#else
#include <QtGui/QGuiApplication>
typedef QGuiApplication Application;
#endif
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtWebEngine/qtwebengineglobal.h>

#include <QFileInfo>

#include "utils.h"

#include <sysexits.h>
#include <iostream>
#include <string>

#include <QDebug>
#include <QJsonParseError>

// Satisfies linker errors
QJsonObject Utils::settings;

/// Displays proper usage instructions
void showInstructions(){
    std::cout << std::endl << "Valid flags for WebViewer:" << std::endl
    << "\t-c {path/to/config/file.conf}" << std::endl
    << "\t-f {path/to/config/file.conf}" << std::endl
    << "\t-c {path/to/config/file.conf}" << std::endl

    << std::endl << "NOTE:" << std::endl
    << "-c is an OPTIONAL argument" << std::endl
    << "-f OR -u is a REQUIRED arguement, but ONLY ONE of them can be used" << std::endl

    << std::endl << "WebView V" << VERSION_MAJOR << "." << VERSION_MINOR << "COPYRIGHT 2015 BMCA Technologies" << std::endl
    << std::endl << "Visit http://www.bmcatech.com/opensource/projects/WebView for more information" << std::endl;
}

/// Returns the Url for a local file, if the file exists, otherwise it attempts to resolve a url from the input
inline QUrl localFileToUrl(const QString & filePath){
    QFileInfo fileInfo(filePath);
    if (fileInfo.exists())
        return QUrl::fromLocalFile(fileInfo.absoluteFilePath());
    return QUrl::fromUserInput(filePath);
}

QUrl startupConfig(QString & configPath, bool & allowMalformedJson){

    qDebug() << "Begining startupConfig";

    QUrl landingPage;

    // Load any command line arguements
    QStringList args(qApp->arguments());
    args.takeFirst();

    enum : char{
        ARG_FILE = 'f',
        ARG_URL = 'u',
        ARG_HELP = 'h',
        ARG_CONFIG = 'c',
        ARG_TITLE = 't',
        ARG_ALLOW_MALFORMED_JSON = 'j',
        ARG_DEBUG = 'd',
        ARG_QUIET = 'q',
    };

    char argType = '\0';
    bool pathGiven = false;

    // Default to false for allowMalformedJson
    allowMalformedJson = false;

    qDebug() << "Begin Command Arguement Parsing";

    /// Parses command line arguments and performs their corresponding tasks
    Q_FOREACH (const QString& arg, args) {
        qDebug() << "Parsing command line argument -> " << arg;

        // Flag found
        if (arg.startsWith(QLatin1Char('-'))){
            // Help Flag
            if(strcmp(arg.toLatin1(), "--help") == 0)
               showInstructions();
            else // Some other flag
                argType = arg.at(1).toLatin1(); // set arguement type
            continue; // skip to the parameter
        }

        // Perform action that corresponds to the argument type (set by most recent flag)
        switch(argType){

        case ARG_CONFIG: // config file given
            configPath = arg;

            // Clear arg type
            argType = '\0';
            break;

        case ARG_FILE: // web file given
            // A startup file/url was already given
            if(pathGiven){
                qFatal("Multiple startup options given...");
                showInstructions();
                exit(EX_USAGE);
            }
            else{
                landingPage = localFileToUrl(arg);

                // Display error if invalid file is given
                if (!landingPage.isValid())
                    qFatal(QString("The file {" + arg + "} is invalid...").toLatin1().data());
                    exit(EX_NOINPUT);

                // Record that a path has been given
                pathGiven = true;

                // Clear arg type
                argType = '\0';
            }
            break;

        case ARG_URL: // url given
            // A startup file/url was already given
            if(pathGiven){
                qFatal("Multiple startup options given...");
                showInstructions();
                exit(EX_USAGE);
            }
            else{ // Startup page has not yet been given
                landingPage = QUrl::fromUserInput(arg);
                pathGiven = true;
            }
            // Clear arg type
            argType = '\0';
            break;

        case ARG_ALLOW_MALFORMED_JSON: // Program is to procede with defaults and warning message after receiving malformed json instead of crashing
            allowMalformedJson = true;
            // Clear arg type
            argType = '\0';
            break;

        default: // mode is not set
            qFatal(QString("No flag given for argument {" + arg + "}\n"
                           + "All arguments MUST have a corresponding flag idenitifier preceding them...").toLatin1().data());
            showInstructions();
            exit(EX_USAGE);
        }
    }

    // Startup page was specified and passed basic verification, return it
    if(pathGiven) {
        qDebug() << "Loading given path/url -> " << landingPage;
        return landingPage;
    }
    else { // No startup page was specificied. Load the default help page
        QUrl url = QUrl("http://bmcatech.com/products/webview");
        qDebug() << "Loading default path/url -> " << url;
        return url;
    }
}
