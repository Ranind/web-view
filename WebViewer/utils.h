/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the QtWebEngine module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file. Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef UTILS_H
#define UTILS_H

#include <QtCore/QFileInfo>
#include <QtCore/QUrl>
#include <QVariant>
#include <QJsonObject>

#include <QFileInfo>

#include "utils.h"

#include <sysexits.h>
#include <iostream>
#include <string>

#include <QDebug>
#include <QJsonParseError>

enum WEBVIEW_VERSION{
    VERSION_MAJOR = 0,
    VERSION_MINOR = 1
};

class Utils : public QObject {
    Q_OBJECT

public:
    static QJsonObject settings;

public:
    Q_INVOKABLE static QUrl fromUserInput(const QString& userInput);
    Q_INVOKABLE static QJsonValue getSetting(const QString& key);

    Utils(QJsonObject settings){
        Utils::settings = settings;
    }

    static void mergeSettings(QJsonObject & settings);
};

inline QUrl Utils::fromUserInput(const QString& userInput)
{
    QFileInfo fileInfo(userInput);
    if (fileInfo.exists())
        return QUrl::fromLocalFile(fileInfo.absoluteFilePath());
    return QUrl::fromUserInput(userInput);
}

inline QJsonValue Utils::getSetting(const QString& key){
    return Utils::settings.value(key);
}

/// Merges json config with defaults for all missing or invalid parameters
/// This emulates the merging strategy of PHP's array_merge()
/// It inserts key-value pairs from the second map into the first, overwriting any existing values
/// Note that this implementation enforces types, if an invalid type is given it will not overwrite the default with it
inline void Utils::mergeSettings(QJsonObject & settings){

    // To allow debug warnings for missing keys without an additional pass, internally this function merges user into defaults

    QVariantMap user = settings.toVariantMap();
    QVariantMap defaults {
        // General Settings

        {"fullscreen", false},
        {"width", 1300},
        {"height", 900},
        {"title", "Webviewer V0.1"},
        {"handle_target_blank_newtab", true},
        {"handle_target_blank_newwindow", false},
        {"handle_target_blank_currenttab", false},
        {"handle_target_blank_ignore", false},

        // Key Binding Settings

        {"bind_back", "Ctrl+B"},
        {"bind_forward", "Ctrl+N"},
        {"bind_newtab", "Ctrl+T"},
        {"bind_closetab", "Ctrl+W"},
        {"bind_exitfullscreen", "Escape"},
        {"bind_enterfullscreen", "F11"},
        {"bind_zoomin", "Ctrl+="},
        {"bind_zoomout", "Ctrl+-"},
        {"bind_zoomreset", "Ctrl+0"},
        {"bind_newwindow", "Ctrl+Shift+T"},
        {"bind_refresh", "Ctrl+R"},
        {"bind_hardrefresh", "Ctrl+Shift+R"},
        {"bind_selectaddressbar", "Ctrl+L"},

        // Modify Action Settings (applies to the right hand dropdown settings only)

        {"modaction_cache", false},
        {"modaction_javascript", false},
        {"modaction_imageautoload", false},
        {"modaction_errorpages", false},
        {"modaction_privatebrowsing", false},

        // Action Settings

        {"action_back", true},
        {"action_forward", true},
        {"action_zoom", true},
        {"action_refresh", true},
        {"action_hardrefresh", true},
        {"action_editaddressbar", true},
        {"action_cache", true},
        {"action_javascript", true},
        {"action_imageautoload", true},
        {"action_errorpages", true},
        {"action_privatebrowsing", false},

        // GUI Settings

        {"gui_toolbar", false},
        {"gui_back", false},
        {"gui_forward", false},
        {"gui_refresh", false},
        {"gui_addressbar", false},
        {"gui_settings", false},
        {"gui_tabs", false}
    };

    enum JSON_TYPES{
        TYPE_BOOL,
        TYPE_INT,
        TYPE_STRING
    };

    QMap<QString, JSON_TYPES> defaultTypes {
        {"fullscreen", TYPE_BOOL},
        {"width", TYPE_INT},
        {"height", TYPE_INT},
        {"title", TYPE_STRING},
        {"handle_target_blank_newtab", TYPE_BOOL},
        {"handle_target_blank_newwindow", TYPE_BOOL},
        {"handle_target_blank_currenttab", TYPE_BOOL},
        {"handle_target_blank_ignore", TYPE_BOOL},

        {"bind_back", TYPE_STRING},
        {"bind_forward", TYPE_STRING},
        {"bind_newtab", TYPE_STRING},
        {"bind_closetab", TYPE_STRING},
        {"bind_exitfullscreen", TYPE_STRING},
        {"bind_enterfullscreen", TYPE_STRING},
        {"bind_zoomin", TYPE_STRING},
        {"bind_zoomout", TYPE_STRING},
        {"bind_zoomreset", TYPE_STRING},
        {"bind_newwindow", TYPE_STRING},
        {"bind_refresh", TYPE_STRING},
        {"bind_hardrefresh", TYPE_STRING},
        {"bind_selectaddressbar", TYPE_STRING},

        {"modaction_cache", TYPE_BOOL},
        {"modaction_javascript", TYPE_BOOL},
        {"modaction_imageautoload", TYPE_BOOL},
        {"modaction_errorpages", TYPE_BOOL},
        {"modaction_privatebrowsing", TYPE_BOOL},

        {"action_back", TYPE_BOOL},
        {"action_forward", TYPE_BOOL},
        {"action_zoom", TYPE_BOOL},
        {"action_refresh", TYPE_BOOL},
        {"action_hardrefresh", TYPE_BOOL},
        {"action_editaddressbar", TYPE_BOOL},
        {"action_cache", TYPE_BOOL},
        {"action_javascript", TYPE_BOOL},
        {"action_imageautoload", TYPE_BOOL},
        {"action_errorpages", TYPE_BOOL},
        {"action_privatebrowsing", TYPE_BOOL},

        {"gui_toolbar", TYPE_BOOL},
        {"gui_back", TYPE_BOOL},
        {"gui_forward", TYPE_BOOL},
        {"gui_refresh", TYPE_BOOL},
        {"gui_addressbar", TYPE_BOOL},
        {"gui_settings", TYPE_BOOL},
        {"gui_tabs", TYPE_BOOL}

    };

    for( auto itr = defaults.begin(); itr != defaults.end(); itr++ ) {
        auto pos = user.find(itr.key());

        // Setting was not provided, use default
        if(pos == user.end()){
            qWarning() << "The key " << itr.key() << " is not provided. Using default";
            qDebug() << "Assigned setting key " << itr.key() << " to value " << itr.value();
        }
        // Setting was provided, check type then overwrite the setting if type is valid
        else{
            auto userval = pos.value();
            auto valtype = defaultTypes.find(itr.key()).value();
            bool validType = false;

            switch(valtype){

            case TYPE_BOOL:
                validType = userval.canConvert(QMetaType::Bool) && userval.convert(QMetaType::Bool);
                break;

            case TYPE_INT:
                validType = userval.canConvert(QMetaType::Int) && userval.convert(QMetaType::Int);
                break;

            case TYPE_STRING:
                validType = userval.canConvert(QMetaType::QString) && userval.convert(QMetaType::QString);
                break;

            }

            // datatype was valid
            if(validType){
                defaults.insert(itr, itr.key(), userval);
                qDebug() << "Assigned setting key " << itr.key() << " to value " << userval;
            }
            // datatype was invalid
            else{
                qWarning() << "The key " << itr.key() << " is provided is the wrong datatype. Using default";
                qDebug() << "Assigned setting key " << itr.key() << " to value " << itr.value();
            }
        }
    }

    settings = QJsonObject::fromVariantMap(defaults);
}

#endif // UTILS_H
