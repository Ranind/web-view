requires(contains(QT_CONFIG, accessibility))

TEMPLATE = app
TARGET = WebView

HEADERS += \
    utils.h
SOURCES += main.cpp \
    startup.cpp

OTHER_FILES += \
    ApplicationRoot.qml \
    BrowserDialog.qml \
    BrowserWindow.qml \
    DownloadView.qml \
    TablessApplicationRoot.qml \
    TablessBroswerWindow.qml

RESOURCES += qml.qrc

QT += qml quick webengine
CONFIG += c++11

qtHaveModule(widgets) {
    QT += widgets # QApplication is required to get native styling with QtQuickControls
}

target.path = WebView
INSTALLS += target

# Additional import path used to resolve QML modules in Qt Creator's code model
#QML_IMPORT_PATH =

# Default rules for deployment.
#include(deployment.pri)

DISTFILES += \
    TablessBrowserWindow.qml \
    TablessApplicationRoot.qml


